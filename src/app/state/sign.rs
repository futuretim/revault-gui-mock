use std::{collections::BTreeMap, sync::Arc, time::Duration};
use std::str::FromStr;

use bitcoin::{blockdata::transaction::OutPoint, util::{
    bip32::{Fingerprint, KeySource},
    psbt::PartiallySignedTransaction as Psbt,
}, Amount, PublicKey, Transaction, TxIn, TxOut, Txid, Script};
use bitcoin::hashes::hex::FromHex;
use bitcoin::hashes::{Hash, hash160, ripemd160, sha256, sha256d};
use bitcoin::util::bip32::ExtendedPubKey;

//use bitcoin::consensus::encode::Error::Psbt;
use bitcoin::util::psbt::{Global, Input, Output, PartiallySignedTransaction, raw};
use tokio::sync::Mutex;

use iced::{time, Command, Element, Subscription};

use revault_hwi::{app::revault::RevaultHWI, HWIError};

use crate::{
    app::{context::Context, error::Error, message::SignMessage, view::sign::SignerView},
    daemon::model::{outpoint, Vault},
};

#[derive(Debug)]
pub struct SpendTransactionTarget {
    pub spend_tx: Psbt,
}

impl SpendTransactionTarget {
    /// Creates a new SpendTransactionTarget to sign with only the corresponding keys of the given
    /// xpubs. The bip32_derivation of the psbt is filtered to possess only the given xpub
    /// fingerprints.
    pub fn new(fingerprints: &Vec<Fingerprint>, mut spend_tx: Psbt) -> Self {
        for input in &mut spend_tx.inputs {
            let mut new_derivation: BTreeMap<PublicKey, KeySource> = BTreeMap::new();
            for (key, source) in &input.bip32_derivation {
                if fingerprints.contains(&source.0) {
                    new_derivation.insert(*key, source.clone());
                }
            }
            input.bip32_derivation = new_derivation;
        }
        Self { spend_tx }
    }
}

#[derive(Debug)]
pub struct Signer<T> {
    device: Device,
    processing: bool,
    signed: bool,

    pub error: Option<Error>,
    pub target: T,

    view: SignerView,
}

impl<T> Signer<T> {
    pub fn new(target: T) -> Self {
        Signer {
            device: Device::new(),
            processing: false,
            signed: false,
            error: None,
            target,
            view: SignerView::new(),
        }
    }

    pub fn signed(&self) -> bool {
        self.signed
    }

    pub fn subscription(&self) -> Subscription<SignMessage> {
        if !self.signed && !self.processing {
            self.device.subscription()
        } else {
            Subscription::none()
        }
    }

    pub fn view(&mut self, ctx: &Context) -> Element<SignMessage> {
        self.view.view(
            ctx,
            self.device.is_connected(),
            self.processing,
            self.signed,
        )
    }
}

impl Signer<SpendTransactionTarget> {
    pub fn update(&mut self, ctx: &Context, message: SignMessage) -> Command<SignMessage> {
        match message {
            SignMessage::SelectSign => {
                self.processing = true;
                return Command::perform(
                    self.device
                        .clone()
                        .sign_spend_tx(self.target.spend_tx.clone()),
                    |tx| SignMessage::PsbtSigned(tx.map(Box::new)),
                );
            }
            SignMessage::PsbtSigned(res) => {
                self.processing = false;
                match res {
                    Ok(tx) => {                        
                            self.signed = true;
                            self.target.spend_tx = *tx;
                    }
                    Err(e) => {
                        log::info!("{:?}", e);
                        self.error = Some(e.into());
                    }
                }
            }
            _ => return self.device.update(&ctx, message),
        };
        Command::none()
    }
}

#[derive(Debug, Clone)]
pub struct Device {
    channel: Option<Arc<Mutex<Box<dyn RevaultHWI + Send>>>>,
}

macro_rules! hex_script {
        ($s: expr) => { Script::from_str($s).expect("") };
    }

impl Device {
    pub fn new() -> Self {
        Device { channel: None }
    }

    pub fn is_connected(&self) -> bool {
        true
    }

    pub fn update(&mut self, ctx: &Context, message: SignMessage) -> Command<SignMessage> {
        match message {
            SignMessage::Ping(res) => {
                if res.is_err() {
                    self.channel = None;
                }
            }
            SignMessage::CheckConnection => {
                if let Some(channel) = self.channel.clone() {
                    return Command::perform(
                        async move { channel.lock().await.is_connected().await },
                        SignMessage::Ping,
                    );
                } else {
                    let connect = &ctx.hardware_wallet;
                    return Command::perform(connect(), |res| {
                        SignMessage::Connected(res.map(|channel| Arc::new(Mutex::new(channel))))
                    });
                }
            }
            SignMessage::Connected(Ok(channel)) => self.channel = Some(channel),
            _ => {}
        };
        Command::none()
    }

    pub fn subscription(&self) -> Subscription<SignMessage> {
        time::every(Duration::from_secs(1)).map(|_| SignMessage::CheckConnection)
    }

    pub async fn sign_revocation_txs(
        self,
        emergency_tx: Psbt,
        emergency_unvault_tx: Psbt,
        cancel_txs: [Psbt; 5],
    ) -> Result<(Psbt, Psbt, [Psbt; 5]), HWIError> {
        let psbt = PartiallySignedTransaction {
            global: Global {
                unsigned_tx: Transaction {
                    version: 2,
                    lock_time: 0,
                    input: vec![],
                    output: vec![],
                },
                xpub: Default::default(),
                version: 0,
                proprietary: BTreeMap::new(),
                unknown: BTreeMap::new(),
            },
            inputs: vec![],
            outputs: vec![],
        };
        Ok((psbt.clone(), psbt.clone(), [psbt.clone(), psbt.clone(), psbt.clone(), psbt.clone(), psbt.clone()]))
    }

    pub async fn sign_unvault_tx(self, unvault_tx: Psbt) -> Result<Psbt, HWIError> {
        if let Some(channel) = self.channel {
            channel.lock().await.sign_unvault_tx(&unvault_tx).await
        } else {
            Err(HWIError::DeviceDisconnected)
        }
    }



    pub async fn sign_spend_tx(self, spend_tx: Psbt) -> Result<Psbt, HWIError> {        
        let tx = Transaction {
            version: 1,
            lock_time: 0,
            input: vec![TxIn {
                previous_output: OutPoint {
                    txid: Txid::from_hex("e567952fb6cc33857f392efa3a46c995a28f69cca4bb1b37e0204dab1ec7a389").unwrap(),
                    vout: 1,
                },
                script_sig: Script::from_str("160014be18d152a9b012039daf3da7de4f53349eecb985").expect(""),
                sequence: 4294967295,
                witness: vec![Vec::from_hex("03d2e15674941bad4a996372cb87e1856d3652606d98562fe39c5e9e7e413f2105").unwrap()],
            }],
            output: vec![
                TxOut {
                    value: 190303501938,
                    script_pubkey: hex_script!("a914339725ba21efd62ac753a9bcd067d6c7a6a39d0587"),
                },
            ],
        };
        let unknown: BTreeMap<raw::Key, Vec<u8>> = vec![(
            raw::Key { type_value: 1, key: vec![0, 1] },
            vec![3, 4 ,5],
        )].into_iter().collect();
        let key_source = ("deadbeef".parse().unwrap(), "m/0'/1".parse().unwrap());
        let keypaths: BTreeMap<PublicKey, KeySource> = vec![(
            "0339880dc92394b7355e3d0439fa283c31de7590812ea011c4245c0674a685e883".parse().unwrap(),
            key_source.clone(),
        )].into_iter().collect();

        let proprietary: BTreeMap<raw::ProprietaryKey, Vec<u8>> = vec![(
            raw::ProprietaryKey {
                prefix: "prefx".as_bytes().to_vec(),
                subtype: 42,
                key: "test_key".as_bytes().to_vec(),
            },
            vec![5, 6, 7],
        )].into_iter().collect();

        let psbt = PartiallySignedTransaction {
            global: Global {
                version: 0,
                xpub: {
                    let xpub: ExtendedPubKey =
                        "xpub661MyMwAqRbcGoRVtwfvzZsq2VBJR1LAHfQstHUoxqDorV89vRoMxUZ27kLrraAj6MPi\
                        QfrDb27gigC1VS1dBXi5jGpxmMeBXEkKkcXUTg4".parse().unwrap();
                    vec![(xpub, key_source.clone())].into_iter().collect()
                },
                unsigned_tx: {
                    let mut unsigned = tx.clone();
                    unsigned.input[0].script_sig = Script::new();
                    unsigned.input[0].witness = Vec::new();
                    unsigned
                },
                proprietary: proprietary.clone(),
                unknown: unknown.clone(),
            },
            inputs: vec![Input {
                non_witness_utxo: Some(tx),
                witness_utxo: Some(TxOut {
                    value: 190303501938,
                    script_pubkey: hex_script!("a914339725ba21efd62ac753a9bcd067d6c7a6a39d0587"),
                }),
                sighash_type: Some("SIGHASH_SINGLE|SIGHASH_ANYONECANPAY".parse().unwrap()),
                redeem_script: Some(vec![0x51].into()),
                witness_script: None,
                partial_sigs: vec![(
                    "0339880dc92394b7355e3d0439fa283c31de7590812ea011c4245c0674a685e883".parse().unwrap(),
                    vec![8, 5, 4],
                )].into_iter().collect(),
                bip32_derivation: keypaths.clone(),
                final_script_witness: Some(vec![vec![1, 3], vec![5]]),
                ripemd160_preimages: vec![(ripemd160::Hash::hash(&[]), vec![1, 2])].into_iter().collect(),
                sha256_preimages: vec![(sha256::Hash::hash(&[]), vec![1, 2])].into_iter().collect(),
                hash160_preimages: vec![(hash160::Hash::hash(&[]), vec![1, 2])].into_iter().collect(),
                hash256_preimages: vec![(sha256d::Hash::hash(&[]), vec![1, 2])].into_iter().collect(),
                proprietary: proprietary.clone(),
                unknown: unknown.clone(),
                ..Default::default()
            }],
            outputs: vec![Output {
                bip32_derivation: keypaths.clone(),
                proprietary: proprietary.clone(),
                unknown: unknown.clone(),
                ..Default::default()
            }],
        };
        let encoded = ::serde_json::to_string(&psbt).unwrap();
        let decoded: PartiallySignedTransaction = ::serde_json::from_str(&encoded).unwrap();

        Ok(psbt)
    }

    pub async fn secure_batch(
        self,
        deposits: &Vec<Vault>,
    ) -> Result<Vec<(Psbt, Psbt, [Psbt; 5])>, HWIError> {
        if let Some(channel) = self.channel {
            let utxos: Vec<(OutPoint, Amount, u32)> = deposits
                .iter()
                .map(|deposit| {
                    (
                        outpoint(deposit),
                        deposit.amount,
                        deposit.derivation_index.into(),
                    )
                })
                .collect();
            channel.lock().await.create_vaults(&utxos).await
        } else {
            Err(HWIError::DeviceDisconnected)
        }
    }

    pub async fn delegate_batch(self, vaults: &Vec<Vault>) -> Result<Vec<Psbt>, HWIError> {
        if let Some(channel) = self.channel {
            let utxos: Vec<(OutPoint, Amount, u32)> = vaults
                .iter()
                .map(|vault| (outpoint(vault), vault.amount, vault.derivation_index.into()))
                .collect();
            channel.lock().await.delegate_vaults(&utxos).await
        } else {
            Err(HWIError::DeviceDisconnected)
        }
    }
}
