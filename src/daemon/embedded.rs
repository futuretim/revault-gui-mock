use std::collections::BTreeMap;
use std::str::FromStr;
use std::sync::Mutex;
use std::time;
// use hash_types::PubkeyHash;
//use hex::FromHex;
use bitcoin::{consensus::encode, util::psbt::PartiallySignedTransaction as Psbt, OutPoint, Txid, Address, hash_types};
use bitcoin::util::{address, amount::Amount, bip32};
use bitcoin::util::psbt::PartiallySignedTransaction;
use log::info;
use super::{model::*, Daemon, RevaultDError};
use revaultd::{
    commands::CommandError,
    commands::HistoryEvent,
    config::Config,
    revault_tx::{
        bitcoin::{
            hashes::hex::{FromHex, ToHex}
        },
        transactions::{
            CancelTransaction, EmergencyTransaction, RevaultTransaction, SpendTransaction,
            UnvaultEmergencyTransaction, UnvaultTransaction
        }
    },
    DaemonHandle,
};
use revaultd::commands::{GetInfoDescriptors, UserRole};
use revaultd::config::WatchtowerConfig;
use revaultd::revault_tx::miniscript::DescriptorPublicKey;
use revaultd::revault_tx::scripts;
use revaultd::revault_tx::scripts::{CpfpDescriptor, DepositDescriptor, DerivedDepositDescriptor, DerivedUnvaultDescriptor, EmergencyAddress, UnvaultDescriptor};
use revaultd::revault_tx::txins::{DepositTxIn, UnvaultTxIn};
use revaultd::revault_tx::txouts::{DepositTxOut, UnvaultTxOut};
use revault_ui::component::form::Value;

impl From<CommandError> for RevaultDError {
    fn from(error: CommandError) -> Self {
        RevaultDError::Rpc(error.code() as i32, error.to_string())
    }
}

pub struct EmbeddedDaemon {
    handle: Option<Mutex<DaemonHandle>>,
}

impl EmbeddedDaemon {
    pub fn new() -> Self {
        Self { handle: None }
    }

    pub fn start(&mut self, config: Config) -> Result<(), RevaultDError> {
        let handle =
            DaemonHandle::start(config).map_err(|e| RevaultDError::Start(e.to_string()))?;
        self.handle = Some(Mutex::new(handle));
        Ok(())
    }
}

impl std::fmt::Debug for EmbeddedDaemon {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("DaemonHandle").finish()
    }
}

pub struct FakeDaemon{
    vaults: Vec<Vault>,
    unvaultTransaction: UnvaultTransaction,
    cancelTransactions: [CancelTransaction; 5]
}

macro_rules! hex_pubkeyhash (($hex:expr) => (bitcoin::PubkeyHash::from_hex(&$hex).unwrap()));

impl FakeDaemon {
    pub fn new() -> Self {
        let cancel_tx1 = CancelTransaction::from_psbt_str("cHNidP8BAF4CAAAAARoHs0elD2sCfWV4+b7PH3aRA+BkRVNf3m/P+Epjx2fNAAAAAAD9////AdLKAgAAAAAAIgAgB6abzQJ4vo5CO9XW3r3JnNumTwlpQbZm9FVICsLHPYQAAAAAAAEBK0ANAwAAAAAAIgAglEs6phQpv+twnAQSdjDvAEic65OtUIijeePBzAAqr50BAwQBAAAAAQWrIQO4lrAuffeRLuEEuwp2hAMZIPmqaHMTUySM3OwdA2hIW6xRh2R2qRTflccImFIy5NdTqwPuPZFB7g1pvYisa3apFOQxXoLeQv/aDFfav/l6YnYRKt+1iKxsk1KHZ1IhA32Q1DEqQ/kUP2MvQYFW46RCexZ5aYk17Arhp01th+37IQNrXQtfIXQdrv+RyyHLilJsb4ujlUMddG9X2jYkeXiWoFKvA3nxALJoIgYCEnb0lxeNI9466wDX+tqlq23zYNacTlLVLLLxtcVobG0INaO2mQoAAAAAAQFHUiED35umh5GhiToV6GS7lTokWfq/Rvy+rRI9XMQuf+foOoEhA9GtXpHhUvxcj9DJWbaRvz59CNsMwH2NEvmRa8gc2WRkUq4iAgISdvSXF40j3jrrANf62qWrbfNg1pxOUtUssvG1xWhsbQg1o7aZCgAAAAA=").unwrap();
        let cancel_tx2 = CancelTransaction::from_psbt_str("cHNidP8BAF4CAAAAARoHs0elD2sCfWV4+b7PH3aRA+BkRVNf3m/P+Epjx2fNAAAAAAD9////AdLKAgAAAAAAIgAgB6abzQJ4vo5CO9XW3r3JnNumTwlpQbZm9FVICsLHPYQAAAAAAAEBK0ANAwAAAAAAIgAglEs6phQpv+twnAQSdjDvAEic65OtUIijeePBzAAqr50BAwQBAAAAAQWrIQO4lrAuffeRLuEEuwp2hAMZIPmqaHMTUySM3OwdA2hIW6xRh2R2qRTflccImFIy5NdTqwPuPZFB7g1pvYisa3apFOQxXoLeQv/aDFfav/l6YnYRKt+1iKxsk1KHZ1IhA32Q1DEqQ/kUP2MvQYFW46RCexZ5aYk17Arhp01th+37IQNrXQtfIXQdrv+RyyHLilJsb4ujlUMddG9X2jYkeXiWoFKvA3nxALJoIgYCEnb0lxeNI9466wDX+tqlq23zYNacTlLVLLLxtcVobG0INaO2mQoAAAAAAQFHUiED35umh5GhiToV6GS7lTokWfq/Rvy+rRI9XMQuf+foOoEhA9GtXpHhUvxcj9DJWbaRvz59CNsMwH2NEvmRa8gc2WRkUq4iAgISdvSXF40j3jrrANf62qWrbfNg1pxOUtUssvG1xWhsbQg1o7aZCgAAAAA=").unwrap();
        let cancel_tx3 = CancelTransaction::from_psbt_str("cHNidP8BAF4CAAAAARoHs0elD2sCfWV4+b7PH3aRA+BkRVNf3m/P+Epjx2fNAAAAAAD9////AdLKAgAAAAAAIgAgB6abzQJ4vo5CO9XW3r3JnNumTwlpQbZm9FVICsLHPYQAAAAAAAEBK0ANAwAAAAAAIgAglEs6phQpv+twnAQSdjDvAEic65OtUIijeePBzAAqr50BAwQBAAAAAQWrIQO4lrAuffeRLuEEuwp2hAMZIPmqaHMTUySM3OwdA2hIW6xRh2R2qRTflccImFIy5NdTqwPuPZFB7g1pvYisa3apFOQxXoLeQv/aDFfav/l6YnYRKt+1iKxsk1KHZ1IhA32Q1DEqQ/kUP2MvQYFW46RCexZ5aYk17Arhp01th+37IQNrXQtfIXQdrv+RyyHLilJsb4ujlUMddG9X2jYkeXiWoFKvA3nxALJoIgYCEnb0lxeNI9466wDX+tqlq23zYNacTlLVLLLxtcVobG0INaO2mQoAAAAAAQFHUiED35umh5GhiToV6GS7lTokWfq/Rvy+rRI9XMQuf+foOoEhA9GtXpHhUvxcj9DJWbaRvz59CNsMwH2NEvmRa8gc2WRkUq4iAgISdvSXF40j3jrrANf62qWrbfNg1pxOUtUssvG1xWhsbQg1o7aZCgAAAAA=").unwrap();
        let cancel_tx4 = CancelTransaction::from_psbt_str("cHNidP8BAF4CAAAAARoHs0elD2sCfWV4+b7PH3aRA+BkRVNf3m/P+Epjx2fNAAAAAAD9////AdLKAgAAAAAAIgAgB6abzQJ4vo5CO9XW3r3JnNumTwlpQbZm9FVICsLHPYQAAAAAAAEBK0ANAwAAAAAAIgAglEs6phQpv+twnAQSdjDvAEic65OtUIijeePBzAAqr50BAwQBAAAAAQWrIQO4lrAuffeRLuEEuwp2hAMZIPmqaHMTUySM3OwdA2hIW6xRh2R2qRTflccImFIy5NdTqwPuPZFB7g1pvYisa3apFOQxXoLeQv/aDFfav/l6YnYRKt+1iKxsk1KHZ1IhA32Q1DEqQ/kUP2MvQYFW46RCexZ5aYk17Arhp01th+37IQNrXQtfIXQdrv+RyyHLilJsb4ujlUMddG9X2jYkeXiWoFKvA3nxALJoIgYCEnb0lxeNI9466wDX+tqlq23zYNacTlLVLLLxtcVobG0INaO2mQoAAAAAAQFHUiED35umh5GhiToV6GS7lTokWfq/Rvy+rRI9XMQuf+foOoEhA9GtXpHhUvxcj9DJWbaRvz59CNsMwH2NEvmRa8gc2WRkUq4iAgISdvSXF40j3jrrANf62qWrbfNg1pxOUtUssvG1xWhsbQg1o7aZCgAAAAA=").unwrap();
        let cancel_tx5 = CancelTransaction::from_psbt_str("cHNidP8BAF4CAAAAARoHs0elD2sCfWV4+b7PH3aRA+BkRVNf3m/P+Epjx2fNAAAAAAD9////AdLKAgAAAAAAIgAgB6abzQJ4vo5CO9XW3r3JnNumTwlpQbZm9FVICsLHPYQAAAAAAAEBK0ANAwAAAAAAIgAglEs6phQpv+twnAQSdjDvAEic65OtUIijeePBzAAqr50BAwQBAAAAAQWrIQO4lrAuffeRLuEEuwp2hAMZIPmqaHMTUySM3OwdA2hIW6xRh2R2qRTflccImFIy5NdTqwPuPZFB7g1pvYisa3apFOQxXoLeQv/aDFfav/l6YnYRKt+1iKxsk1KHZ1IhA32Q1DEqQ/kUP2MvQYFW46RCexZ5aYk17Arhp01th+37IQNrXQtfIXQdrv+RyyHLilJsb4ujlUMddG9X2jYkeXiWoFKvA3nxALJoIgYCEnb0lxeNI9466wDX+tqlq23zYNacTlLVLLLxtcVobG0INaO2mQoAAAAAAQFHUiED35umh5GhiToV6GS7lTokWfq/Rvy+rRI9XMQuf+foOoEhA9GtXpHhUvxcj9DJWbaRvz59CNsMwH2NEvmRa8gc2WRkUq4iAgISdvSXF40j3jrrANf62qWrbfNg1pxOUtUssvG1xWhsbQg1o7aZCgAAAAA=").unwrap();

        Self {
            cancelTransactions: [cancel_tx1, cancel_tx2, cancel_tx3, cancel_tx4, cancel_tx5],
            unvaultTransaction: UnvaultTransaction::from_psbt_str("cHNidP8BAIkCAAAAAajRZE5yVgzG9McmOyy/WdcYdrGrK15bB5N/Hg8zhKOkAQAAAAD9////ArhhpDUAAAAAIgAgFZlOQkpDkFSsLUfyeMGVAOT3T88jZM7L/XlVZoJ2jnAwdQAAAAAAACIAILKCCA/RbV3QMPMrwwQmk4Ark4w1WyElM27WtBgftq6ZAAAAAAABASsA6aQ1AAAAACIAIPQJ3LCGXPIO5iXX0/Yp3wHlpao7cQbPd4q3gxp0J/w2AQMEAQAAAAEFR1IhA47+JRqdt+oloFosla9hWUYVf5YQKDbuq4KO13JS45KgIQMKcLWzABxb/9YBQe+bJRW3v3om8S2LNMGUKSp5K+PQ+1KuIgYCEnb0lxeNI9466wDX+tqlq23zYNacTlLVLLLxtcVobG0INaO2mQoAAAAAAQGpIQMVlEoh50lasMhcdwnrmnCp2ROlGY5CrH+HtxQmfZDZ06xRh2R2qRS/INUX1CaP7Pbn5GmtGYu2wgqjnIisa3apFO/kceq8yo9w69g4VVtlFAf739qTiKxsk1KHZ1IhAnddfXi3N38A+aEQ74sUdeuV7sg+2L3ijTjMHMEAfq3cIQLWP96FqjfC5qKQkC2WhYbbLJx1FbNSAjsnMfwDnK0jD1KvARKyaCICAhJ29JcXjSPeOusA1/rapatt82DWnE5S1Syy8bXFaGxtCDWjtpkKAAAAAAEBJSEDjv4lGp236iWgWiyVr2FZRhV/lhAoNu6rgo7XclLjkqCsUYciAgISdvSXF40j3jrrANf62qWrbfNg1pxOUtUssvG1xWhsbQg1o7aZCgAAAAA=").unwrap(),
            vaults: vec![
                Vault {
                    address: Address::from_str(
                        "tb1qkldgvljmjpxrjq2ev5qxe8dvhn0dph9q85pwtfkjeanmwdue2akqj4twxj"
                    )
                        .unwrap(),
                    amount: Amount::from_sat(500),
                    derivation_index: bip32::ChildNumber::from_normal_idx(0).unwrap(),
                    status: VaultStatus::Unvaulting,
                    txid: bitcoin::Txid::from_str(
                        "a1075db55d416d3ca199f55b6084e2115b9345e16c5cf302fc80e9d5fbf5d48d"
                    )
                        .unwrap(),
                    vout: 0,
                    blockheight: Some(1),
                    delegated_at: None,
                    secured_at: Some(1),
                    funded_at: Some(1),
                    moved_at: None
                },
                Vault {
                    address: Address::from_str(
                        "tb1qkldgvljmjpxrjq2ev5qxe8dvhn0dph9q85pwtfkjeanmwdue2akqj4twxj"
                    )
                        .unwrap(),
                    amount: Amount::from_sat(700),
                    derivation_index: bip32::ChildNumber::from_normal_idx(0).unwrap(),
                    status: VaultStatus::Unvaulted,
                    txid: bitcoin::Txid::from_str(
                        "a1075db55d416d3ca199f55b6084e2115b9345e16c5cf302fc80e9d5fbf5d48d"
                    )
                        .unwrap(),
                    vout: 1,
                    blockheight: Some(1),
                    delegated_at: None,
                    secured_at: Some(1),
                    funded_at: Some(1),
                    moved_at: None
                },
                Vault {
                    address: Address::from_str(
                        "tb1qkldgvljmjpxrjq2ev5qxe8dvhn0dph9q85pwtfkjeanmwdue2akqj4twxj"
                    )
                        .unwrap(),
                    amount: Amount::from_sat(700),
                    derivation_index: bip32::ChildNumber::from_normal_idx(0).unwrap(),
                    status: VaultStatus::Unvaulted,
                    txid: bitcoin::Txid::from_str(
                        "a1075db55d416d3ca199f55b6084e2115b9345e16c5cf302fc80e9d5fbf5d48d"
                    )
                        .unwrap(),
                    vout: 2,
                    blockheight: Some(1),
                    delegated_at: None,
                    secured_at: Some(1),
                    funded_at: Some(1),
                    moved_at: None
                },
                Vault {
                    address: Address::from_str(
                        "tb1qkldgvljmjpxrjq2ev5qxe8dvhn0dph9q85pwtfkjeanmwdue2akqj4twxj"
                    )
                        .unwrap(),
                    amount: Amount::from_sat(1000000),
                    derivation_index: bip32::ChildNumber::from_normal_idx(0).unwrap(),
                    status: VaultStatus::Unvaulted,
                    txid: bitcoin::Txid::from_str(
                        "a1075db55d416d3ca199f55b6084e2115b9345e16c5cf302fc80e9d5fbf5d48d"
                    )
                        .unwrap(),
                    vout: 2,
                    blockheight: Some(1),
                    delegated_at: None,
                    secured_at: Some(1),
                    funded_at: Some(1),
                    moved_at: None
                }
            ]
        }
    }
}

impl Daemon for FakeDaemon {

    fn is_external(&self) -> bool {
        false
    }

    fn load_config(&mut self, _cfg: Config) -> Result<(), RevaultDError> {
        Ok(())
    }

    fn stop(&mut self) -> Result<(), RevaultDError> {
        Ok(())
    }

    fn get_deposit_address(&self) -> Result<Address, RevaultDError> {
        let address = Address{ payload: address::Payload::PubkeyHash(Default::default()), network: bitcoin::Network::Bitcoin };
        Ok(address)
    }

    fn get_info(&self) -> Result<GetInfoResult, RevaultDError> {
        let first_stakeholder = DescriptorPublicKey::from_str("xpub6EHLFGpTTiZgHAHfBJ1LoepGFX5iyLeZ6CVtF9HhzeB1dkxLsEfkiJda78EKhSXuo2m8gQwAs4ZAbqaJixFYHMFWTL9DJX1KsAXS2VY5JJx/*").unwrap();
        let second_stakeholder = DescriptorPublicKey::from_str("xpub6F2U61Uh9FNX94mZE6EgdZ3p5Wg8af6MHzFhskEskkAZ9ns2uvsnHBskU47wYY63yiYv8WufvTuHCePwUjK9zhKT1Cce8JGLBptncpvALw6/*").unwrap();

        let deposit_descriptor =
            DepositDescriptor::new(vec![first_stakeholder, second_stakeholder]).expect("Compiling descriptor");


        let first_manager = DescriptorPublicKey::from_str("xpub6Duq1ob3cQ8Wxees2fTGNK2wTsVjgTPQcKJiPquXY2rQJTDjeCxkXFxTCGhcunFDt26Ddz45KQu7pbLmmUGG2PXTRVx3iDpBPEhdrijJf4U/*").unwrap();
        let second_manager = DescriptorPublicKey::from_str("xpub6EWL35hY9uZZs5Ljt6J3G2ZK1Tu4GPVkFdeGvMknG3VmwVRHhtadCaw5hdRDBgrmx1nPVHWjGBb5xeuC1BfbJzjjcic2gNm1aA7ywWjj7G8/*").unwrap();

        let first_stakeholder2 = DescriptorPublicKey::from_str("xpub6EHLFGpTTiZgHAHfBJ1LoepGFX5iyLeZ6CVtF9HhzeB1dkxLsEfkiJda78EKhSXuo2m8gQwAs4ZAbqaJixFYHMFWTL9DJX1KsAXS2VY5JJx/*").unwrap();
        let second_stakeholder2 = DescriptorPublicKey::from_str("xpub6F2U61Uh9FNX94mZE6EgdZ3p5Wg8af6MHzFhskEskkAZ9ns2uvsnHBskU47wYY63yiYv8WufvTuHCePwUjK9zhKT1Cce8JGLBptncpvALw6/*").unwrap();


        let unvault_descriptor = UnvaultDescriptor::new(
            vec![first_stakeholder2, second_stakeholder2],
            vec![first_manager, second_manager],
            1,
            // No cosigning server
            vec![],
            // CSV
            42
        ).expect("Compiling descriptor");
        //println!("Unvault descriptor: {}", unvault_descriptor);

        let first_manager2 = DescriptorPublicKey::from_str("xpub6EHLFGpTTiZgHAHfBJ1LoepGFX5iyLeZ6CVtF9HhzeB1dkxLsEfkiJda78EKhSXuo2m8gQwAs4ZAbqaJixFYHMFWTL9DJX1KsAXS2VY5JJx/*").unwrap();
        let second_manager2 = DescriptorPublicKey::from_str("xpub6F2U61Uh9FNX94mZE6EgdZ3p5Wg8af6MHzFhskEskkAZ9ns2uvsnHBskU47wYY63yiYv8WufvTuHCePwUjK9zhKT1Cce8JGLBptncpvALw6/*").unwrap();

        let cpfp_descriptor =
            CpfpDescriptor::new(vec![first_manager2, second_manager2]).expect("Compiling descriptor");
        //println!("CPFP descriptor: {}", cpfp_descriptor);

        let info = GetInfoResult{
            version: "".to_string(),
            network: bitcoin::Network::Bitcoin,
            blockheight: 1,
            sync: f64::EPSILON + 1.0_f64,
            vaults: self.vaults.len(),
            managers_threshold: 1,
            descriptors: GetInfoDescriptors {
                deposit: deposit_descriptor,
                unvault: unvault_descriptor,
                cpfp: cpfp_descriptor,
            },
            participant_type: UserRole::Stakeholder,
        };

        Ok(info)
    }

    fn list_vaults(&self, statuses: Option<&[VaultStatus]>, outpoints: Option<&[OutPoint]>) -> Result<Vec<Vault>, RevaultDError> {
        Ok(self.vaults.clone())
    }

    fn list_onchain_transactions(&self, outpoints: &[OutPoint]) -> Result<Vec<VaultTransactions>, RevaultDError> {
        let spend_tx_hex = "0200000001b4243a48b54cc360e754e0175a985a49b67cf4615d8523ec5aa46d42421cdf7d0000000000504200000280b2010000000000220020b9be8f8574f8da64bb1cb6668f6134bc4706df7936eeab8411f9d82de20a895b08280954020000000000000000";

        let txs : Vec<VaultTransactions> = vec![VaultTransactions{
            vault_outpoint: Default::default(),
            deposit: WalletTransaction {
                hex: spend_tx_hex.to_string(),
                received_time: 0,
                blockheight: None,
                blocktime: None,
            },
            unvault: None,
            cancel: None,
            emergency: None,
            unvault_emergency: None,
            spend: None,
        }];
        Ok(txs)
    }

    fn list_presigned_transactions(&self, outpoints: &[OutPoint]) -> Result<Vec<VaultPresignedTransactions>, RevaultDError> {
        let txs : Vec<VaultPresignedTransactions> = vec![
            VaultPresignedTransactions{
                vault_outpoint: Default::default(),
                unvault: self.unvaultTransaction.clone(),
                cancel: self.cancelTransactions.clone(),
                emergency: None,
                unvault_emergency: None,
            }
        ];
        Ok(txs)
    }

    fn get_revocation_txs(&self, outpoint: &OutPoint) -> Result<RevocationTransactions, RevaultDError> {
        let addr = Address {
            network: bitcoin::Network::Bitcoin,
            payload: address::Payload::PubkeyHash(hex_pubkeyhash!("162c5ea71c0b23f5b9022ef047c4a86470a5b070")),
        };

        let emergencyAddress = EmergencyAddress::from(addr).expect("");

        let derivedDD = DerivedDepositDescriptor::new(vec![]).expect("");

        let depositTxOut = DepositTxOut::new(Default::default(), &derivedDD);

        let emergencyTx = EmergencyTransaction::new(
            DepositTxIn::new(OutPoint{ txid: Default::default(), vout: 0 }, depositTxOut
            ),
            emergencyAddress.clone()
        ).expect("");

        let first_stakeholder = scripts::DerivedPublicKey::from_str("[21212121/21]0372f4bb19ecf98d7849148b4f40375d2fcef624a1b56fef94489ad012bc11b4df").unwrap();
        let second_stakeholder = scripts::DerivedPublicKey::from_str("[10000000/1]036e7ac7a096270f676b53e9917942cf42c6fb9607e3bc09775b5209c908525e80").unwrap();
        let third_stakeholder = scripts::DerivedPublicKey::from_str("[ffffffff/4]03a02e93cf8c47b250075b0af61f96ebd10376c0aaa7635148e889cb2b51c96927").unwrap();

        let first_manager = scripts::DerivedPublicKey::from_str("[fafafafa/21]03d33a510c0376a3d19ffa0e1ba71d5ee0cbfebbce2df0996b51262142e943c6f0").unwrap();
        let second_manager = scripts::DerivedPublicKey::from_str("[fafafafa/21]030e7d7e1d8014dc17d63057ffc3ef26590bf237ce50054fb4f612be8e0a0dbe2a").unwrap();


        let derivedUD = DerivedUnvaultDescriptor::new(
            vec![first_stakeholder, second_stakeholder, third_stakeholder],
            vec![first_manager, second_manager],
            1,
            vec![],
            42
        ).expect("");
        let unvaultTxOut = UnvaultTxOut::new(Default::default(), &derivedUD);

        let unvaultTxIn = UnvaultTxIn::new(OutPoint { txid: Default::default(), vout: 0 }, unvaultTxOut, 0);

        let unvaultEmerTx = UnvaultEmergencyTransaction::new(unvaultTxIn, emergencyAddress).expect("");

        let revTxs = RevocationTransactions{
             cancel_txs: self.cancelTransactions.clone(),
             emergency_tx: emergencyTx,
             emergency_unvault_tx: unvaultEmerTx,
        };

        Ok(revTxs)
    }

    fn set_revocation_txs(&self, outpoint: &OutPoint, emergency_tx: &Psbt, emergency_unvault_tx: &Psbt, cancel_tx: &[Psbt; 5]) -> Result<(), RevaultDError> {
        Ok(())
    }

    fn get_unvault_tx(&self, outpoint: &OutPoint) -> Result<Psbt, RevaultDError> {
        todo!()
    }

    fn set_unvault_tx(&self, outpoint: &OutPoint, unvault_tx: &Psbt) -> Result<(), RevaultDError> {
        Ok(())
    }

    fn get_spend_tx(&self, inputs: &[OutPoint], outputs: &BTreeMap<Address, u64>, feerate: u64) -> Result<SpendTx, RevaultDError> {
        let spend = SpendTransaction::from_psbt_str("cHNidP8BAP0ZAQIAAAADSe9QbkOAlLapVzhNT2J2sCWXMqe2x7g7klEr3N6+p8AAAAAAAAYAAABwwCBKiamcBn5fj0oQ3WcAU+twE4XemcK4G2nlprqBKAAAAAAABgAAAAwCYIUh0y2bkIH8BVZ/evuFulOCxOyGr/rvZnR2k/9aAAAAAAAGAAAABFCoAAAAAAAAIgAgvXwvxBU2X03+pufsytFJ2dS4BKVXIKMQmyxUXTbPJPmA8PoCAAAAABYAFCMDXGnefAb487YxeNjnpbUzH+pEQEtMAAAAAAAWABT+rq2LTo+bnAo3ZQoeUg0F6xVZbIx3EwIAAAAAIgAgfAYV/vqzwEWHS6kVMjA1yQRbIQqq//o7m4ik0eSSlasAAAAAAAEBKzb0VQMAAAAAIgAgEyIAQqFnv+D0rMmVvusK3TC6fPyFk7aU1PZ8+Ttm23IBAwQBAAAAAQXBUiEDNWoO4CCloCul5eCLd1+XLPxP4LMIsUy+XM01wlm59wIhAqQ3tGeAeMBPPR26fn0kuL0CS0AybrDlu8NwIzFOOukzIQJoBBIwDWTXwjMse2MiB8/kIcFOZACiADcmZltiEl85N1OuZHapFIe9/DRONZOp5OAQ6RCrIDclCDEjiKxrdqkUJs2E27SQYhbh4yxNkO+lDnFqCCaIrGyTa3apFBtcD9uL3TRJt1uCIj2J8Ub4YjvgiKxsk1OHZ1ayaCIGAmgEEjANZNfCMyx7YyIHz+QhwU5kAKIANyZmW2ISXzk3CO9FHBcBAAAAIgYCpDe0Z4B4wE89Hbp+fSS4vQJLQDJusOW7w3AjMU466TMIW+FtfgEAAAAiBgM1ag7gIKWgK6Xl4It3X5cs/E/gswixTL5czTXCWbn3AgjDFaC/AQAAAAABASs2rG0BAAAAACIAIICUwlAfLlUkhU44Hpkj/LEDNAdwME4fm3jtWfXwMwL7AQMEAQAAAAEFwVIhAgSNQIWSNnYSrfEl8juzTKw9o3BjYQ+DgbyizShqKzIcIQN+tRtybpIxVK9IdwxsTxFgy2YsiQqtnGvnowXelPblJiEC25bXunBKDpmrAvXiBbJ/+x9Oo5pL+8FhKgAqXSesn0VTrmR2qRTWWGTXm1UxE4rqqD2FkiKS94r8YYisa3apFCBven2wd5QCFoHAl/iRHg+9SJkgiKxsk2t2qRRP/mE3OesTO6kSJOgsBAoyLTfO8oisbJNTh2dWsmgiBgIEjUCFkjZ2Eq3xJfI7s0ysPaNwY2EPg4G8os0oaisyHAjDFaC/AAAAACIGAtuW17pwSg6ZqwL14gWyf/sfTqOaS/vBYSoAKl0nrJ9FCO9FHBcAAAAAIgYDfrUbcm6SMVSvSHcMbE8RYMtmLIkKrZxr56MF3pT25SYIW+FtfgAAAAAAAQErtgyYAAAAAAAiACCAlMJQHy5VJIVOOB6ZI/yxAzQHcDBOH5t47Vn18DMC+wEDBAEAAAABBcFSIQIEjUCFkjZ2Eq3xJfI7s0ysPaNwY2EPg4G8os0oaisyHCEDfrUbcm6SMVSvSHcMbE8RYMtmLIkKrZxr56MF3pT25SYhAtuW17pwSg6ZqwL14gWyf/sfTqOaS/vBYSoAKl0nrJ9FU65kdqkU1lhk15tVMROK6qg9hZIikveK/GGIrGt2qRQgb3p9sHeUAhaBwJf4kR4PvUiZIIisbJNrdqkUT/5hNznrEzupEiToLAQKMi03zvKIrGyTU4dnVrJoIgYCBI1AhZI2dhKt8SXyO7NMrD2jcGNhD4OBvKLNKGorMhwIwxWgvwAAAAAiBgLblte6cEoOmasC9eIFsn/7H06jmkv7wWEqACpdJ6yfRQjvRRwXAAAAACIGA361G3JukjFUr0h3DGxPEWDLZiyJCq2ca+ejBd6U9uUmCFvhbX4AAAAAACICArFlfWaPsqMsvdC+/3Hise+ubUHtj4n5Uz7qaI0bCfCWCBhRVloBAAAAIgICtg6ewcvt4XnF35qT+j9KoCYt4+vS8hXmOn1NsO/QppUIgryGbgEAAAAiAgOJmnB0i/XOb8ITGRA3itrYfvWx6/B8PGMiu2SYfOACFQhTR/BbAQAAAAAAACICAr+BTfGuO1VRPxE1DJoFIsH1Vu5Dk5lSullVQjCXjVlICEPuDksBAAAAIgIC+G7/TA9DNgnMf4Nup2Py3XAF8UCLmziV3Vw4Z2KsJcwIpbzhFQEAAAAiAgOpos5KhVRQaTPJTi3mk12g5sApoQNVGdOpMcMmn7C7gwieIH0+AQAAAAA=").unwrap();
        Ok(SpendTx::from(ListSpendEntry{
            deposit_outpoints: vec![],
            deposit_amount: Amount::from_sat(100),
            cpfp_amount: Amount::from_sat(5),
            psbt: spend,
            cpfp_index: 0,
            change_index: None,
            status: ListSpendStatus::NonFinal,
        }))
    }

    fn update_spend_tx(&self, psbt: &Psbt) -> Result<(), RevaultDError> {
        Ok(())
    }

    fn list_spend_txs(&self, statuses: Option<&[SpendTxStatus]>) -> Result<Vec<SpendTx>, RevaultDError> {
        let spend = SpendTransaction::from_psbt_str("cHNidP8BAP0ZAQIAAAADSe9QbkOAlLapVzhNT2J2sCWXMqe2x7g7klEr3N6+p8AAAAAAAAYAAABwwCBKiamcBn5fj0oQ3WcAU+twE4XemcK4G2nlprqBKAAAAAAABgAAAAwCYIUh0y2bkIH8BVZ/evuFulOCxOyGr/rvZnR2k/9aAAAAAAAGAAAABFCoAAAAAAAAIgAgvXwvxBU2X03+pufsytFJ2dS4BKVXIKMQmyxUXTbPJPmA8PoCAAAAABYAFCMDXGnefAb487YxeNjnpbUzH+pEQEtMAAAAAAAWABT+rq2LTo+bnAo3ZQoeUg0F6xVZbIx3EwIAAAAAIgAgfAYV/vqzwEWHS6kVMjA1yQRbIQqq//o7m4ik0eSSlasAAAAAAAEBKzb0VQMAAAAAIgAgEyIAQqFnv+D0rMmVvusK3TC6fPyFk7aU1PZ8+Ttm23IBAwQBAAAAAQXBUiEDNWoO4CCloCul5eCLd1+XLPxP4LMIsUy+XM01wlm59wIhAqQ3tGeAeMBPPR26fn0kuL0CS0AybrDlu8NwIzFOOukzIQJoBBIwDWTXwjMse2MiB8/kIcFOZACiADcmZltiEl85N1OuZHapFIe9/DRONZOp5OAQ6RCrIDclCDEjiKxrdqkUJs2E27SQYhbh4yxNkO+lDnFqCCaIrGyTa3apFBtcD9uL3TRJt1uCIj2J8Ub4YjvgiKxsk1OHZ1ayaCIGAmgEEjANZNfCMyx7YyIHz+QhwU5kAKIANyZmW2ISXzk3CO9FHBcBAAAAIgYCpDe0Z4B4wE89Hbp+fSS4vQJLQDJusOW7w3AjMU466TMIW+FtfgEAAAAiBgM1ag7gIKWgK6Xl4It3X5cs/E/gswixTL5czTXCWbn3AgjDFaC/AQAAAAABASs2rG0BAAAAACIAIICUwlAfLlUkhU44Hpkj/LEDNAdwME4fm3jtWfXwMwL7AQMEAQAAAAEFwVIhAgSNQIWSNnYSrfEl8juzTKw9o3BjYQ+DgbyizShqKzIcIQN+tRtybpIxVK9IdwxsTxFgy2YsiQqtnGvnowXelPblJiEC25bXunBKDpmrAvXiBbJ/+x9Oo5pL+8FhKgAqXSesn0VTrmR2qRTWWGTXm1UxE4rqqD2FkiKS94r8YYisa3apFCBven2wd5QCFoHAl/iRHg+9SJkgiKxsk2t2qRRP/mE3OesTO6kSJOgsBAoyLTfO8oisbJNTh2dWsmgiBgIEjUCFkjZ2Eq3xJfI7s0ysPaNwY2EPg4G8os0oaisyHAjDFaC/AAAAACIGAtuW17pwSg6ZqwL14gWyf/sfTqOaS/vBYSoAKl0nrJ9FCO9FHBcAAAAAIgYDfrUbcm6SMVSvSHcMbE8RYMtmLIkKrZxr56MF3pT25SYIW+FtfgAAAAAAAQErtgyYAAAAAAAiACCAlMJQHy5VJIVOOB6ZI/yxAzQHcDBOH5t47Vn18DMC+wEDBAEAAAABBcFSIQIEjUCFkjZ2Eq3xJfI7s0ysPaNwY2EPg4G8os0oaisyHCEDfrUbcm6SMVSvSHcMbE8RYMtmLIkKrZxr56MF3pT25SYhAtuW17pwSg6ZqwL14gWyf/sfTqOaS/vBYSoAKl0nrJ9FU65kdqkU1lhk15tVMROK6qg9hZIikveK/GGIrGt2qRQgb3p9sHeUAhaBwJf4kR4PvUiZIIisbJNrdqkUT/5hNznrEzupEiToLAQKMi03zvKIrGyTU4dnVrJoIgYCBI1AhZI2dhKt8SXyO7NMrD2jcGNhD4OBvKLNKGorMhwIwxWgvwAAAAAiBgLblte6cEoOmasC9eIFsn/7H06jmkv7wWEqACpdJ6yfRQjvRRwXAAAAACIGA361G3JukjFUr0h3DGxPEWDLZiyJCq2ca+ejBd6U9uUmCFvhbX4AAAAAACICArFlfWaPsqMsvdC+/3Hise+ubUHtj4n5Uz7qaI0bCfCWCBhRVloBAAAAIgICtg6ewcvt4XnF35qT+j9KoCYt4+vS8hXmOn1NsO/QppUIgryGbgEAAAAiAgOJmnB0i/XOb8ITGRA3itrYfvWx6/B8PGMiu2SYfOACFQhTR/BbAQAAAAAAACICAr+BTfGuO1VRPxE1DJoFIsH1Vu5Dk5lSullVQjCXjVlICEPuDksBAAAAIgIC+G7/TA9DNgnMf4Nup2Py3XAF8UCLmziV3Vw4Z2KsJcwIpbzhFQEAAAAiAgOpos5KhVRQaTPJTi3mk12g5sApoQNVGdOpMcMmn7C7gwieIH0+AQAAAAA=").unwrap();
        let spendTxs : Vec<SpendTx> = vec![
            SpendTx {
                deposit_outpoints: vec![],
                deposit_amount: Default::default(),
                cpfp_amount: Default::default(),
                psbt: spend,
                cpfp_index: 0,
                change_index: None,
                status: ListSpendStatus::NonFinal,
            }
        ];
        Ok(spendTxs)
    }

    fn delete_spend_tx(&self, txid: &Txid) -> Result<(), RevaultDError> {
        Ok(())
    }

    fn broadcast_spend_tx(&self, txid: &Txid, priority: bool) -> Result<(), RevaultDError> {
        info!("broadcasting! {}", txid);
        Ok(())
    }

    fn revault(&self, outpoint: &OutPoint) -> Result<(), RevaultDError> {
        Ok(())
    }

    fn emergency(&self) -> Result<(), RevaultDError> {
        Ok(())
    }

    fn get_server_status(&self) -> Result<ServersStatuses, RevaultDError> {
        Ok(ServersStatuses{
            coordinator: ServerStatus { host: "random-string".to_string(), reachable: true },
            cosigners: vec![],
            watchtowers: vec![
                ServerStatus{
                    host: "whatever".to_string(),
                    reachable: true
                }
            ],
        })
    }

    fn get_history(&self, kind: &[HistoryEventKind], start: u32, end: u32, limit: u64) -> Result<Vec<HistoryEvent>, RevaultDError> {
        let history: Vec<HistoryEvent> = vec![
            HistoryEvent {
                blockheight: 1,
                date: 1,
                txid: bitcoin::Txid::from_str(
                    "a1075db55d416d3ca199f55b6084e2115b9345e16c5cf302fc80e9d5fbf5d48d"
                )
                    .unwrap(),
                kind: HistoryEventKind::Spend,
                amount: Some(1_000_000),
                miner_fee: Some(2000),
                cpfp_amount: Some(3000),
                vaults: vec![OutPoint{ txid: Txid::from_str("a1075db55d416d3ca199f55b6084e2115b9345e16c5cf302fc80e9d5fbf5d48d").expect(""), vout: 0 }]
            },
        ];
        Ok(history)
    }
}

impl std::fmt::Debug for FakeDaemon {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("DaemonHandle").finish()
    }
}

impl Daemon for EmbeddedDaemon {
    fn is_external(&self) -> bool {
        false
    }

    fn load_config(&mut self, cfg: Config) -> Result<(), RevaultDError> {
        if self.handle.is_none() {
            return Ok(());
        }

        let next = DaemonHandle::start(cfg).map_err(|e| RevaultDError::Start(e.to_string()))?;
        self.handle.take().unwrap().into_inner().unwrap().shutdown();
        self.handle = Some(Mutex::new(next));
        Ok(())
    }

    fn stop(&mut self) -> Result<(), RevaultDError> {
        if let Some(h) = self.handle.take() {
            let handle = h.into_inner().unwrap();
            handle.shutdown();
        }
        Ok(())
    }

    fn get_deposit_address(&self) -> Result<bitcoin::Address, RevaultDError> {
        Ok(self
            .handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .get_deposit_address())
    }

    fn get_info(&self) -> Result<GetInfoResult, RevaultDError> {
        Ok(self
            .handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .get_info())
    }

    fn list_vaults(
        &self,
        statuses: Option<&[VaultStatus]>,
        outpoints: Option<&[OutPoint]>,
    ) -> Result<Vec<Vault>, RevaultDError> {
        Ok(self
            .handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .list_vaults(statuses, outpoints))
    }

    fn list_onchain_transactions(
        &self,
        outpoints: &[OutPoint],
    ) -> Result<Vec<VaultTransactions>, RevaultDError> {
        self.handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .list_onchain_txs(outpoints)
            .map_err(|e| e.into())
    }

    fn list_presigned_transactions(
        &self,
        outpoints: &[OutPoint],
    ) -> Result<Vec<VaultPresignedTransactions>, RevaultDError> {
        self.handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .list_presigned_txs(outpoints)
            .map_err(|e| e.into())
    }

    fn get_revocation_txs(
        &self,
        outpoint: &OutPoint,
    ) -> Result<RevocationTransactions, RevaultDError> {
        self.handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .get_revocation_txs(*outpoint)
            .map_err(|e| e.into())
    }

    fn set_revocation_txs(
        &self,
        outpoint: &OutPoint,
        emergency_tx: &Psbt,
        emergency_unvault_tx: &Psbt,
        cancel_txs: &[Psbt; 5],
    ) -> Result<(), RevaultDError> {
        let cancel_txs: [CancelTransaction; 5] = [
            CancelTransaction::from_raw_psbt(&encode::serialize(&cancel_txs[0])).unwrap(),
            CancelTransaction::from_raw_psbt(&encode::serialize(&cancel_txs[1])).unwrap(),
            CancelTransaction::from_raw_psbt(&encode::serialize(&cancel_txs[2])).unwrap(),
            CancelTransaction::from_raw_psbt(&encode::serialize(&cancel_txs[3])).unwrap(),
            CancelTransaction::from_raw_psbt(&encode::serialize(&cancel_txs[4])).unwrap(),
        ];
        let emergency_tx =
            EmergencyTransaction::from_raw_psbt(&encode::serialize(emergency_tx)).unwrap();
        let emergency_unvault_tx =
            UnvaultEmergencyTransaction::from_raw_psbt(&encode::serialize(emergency_unvault_tx))
                .unwrap();
        self.handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .set_revocation_txs(
                *outpoint,
                RevocationTransactions {
                    cancel_txs,
                    emergency_tx,
                    emergency_unvault_tx,
                },
            )
            .map_err(|e| e.into())
    }

    fn get_unvault_tx(&self, outpoint: &OutPoint) -> Result<Psbt, RevaultDError> {
        self.handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .get_unvault_tx(*outpoint)
            .map(|tx| tx.into_psbt())
            .map_err(|e| e.into())
    }

    fn set_unvault_tx(&self, outpoint: &OutPoint, unvault_tx: &Psbt) -> Result<(), RevaultDError> {
        let unvault = UnvaultTransaction::from_raw_psbt(&encode::serialize(unvault_tx)).unwrap();
        self.handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .set_unvault_tx(*outpoint, unvault)
            .map_err(|e| e.into())
    }

    fn get_spend_tx(
        &self,
        inputs: &[OutPoint],
        outputs: &BTreeMap<bitcoin::Address, u64>,
        feerate: u64,
    ) -> Result<SpendTx, RevaultDError> {
        self.handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .get_spend_tx(inputs, outputs, feerate)
            .map_err(|e| e.into())
    }

    fn update_spend_tx(&self, psbt: &Psbt) -> Result<(), RevaultDError> {
        let spend = SpendTransaction::from_raw_psbt(&encode::serialize(psbt)).unwrap();
        self.handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .update_spend_tx(spend)
            .map_err(|e| e.into())
    }

    fn list_spend_txs(
        &self,
        statuses: Option<&[SpendTxStatus]>,
    ) -> Result<Vec<SpendTx>, RevaultDError> {
        self.handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .list_spend_txs(statuses)
            .map_err(|e| e.into())
    }

    fn delete_spend_tx(&self, txid: &Txid) -> Result<(), RevaultDError> {
        self.handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .del_spend_tx(txid)
            .map_err(|e| e.into())
    }

    fn broadcast_spend_tx(&self, txid: &Txid, priority: bool) -> Result<(), RevaultDError> {
        self.handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .set_spend_tx(txid, priority)
            .map_err(|e| e.into())
    }

    fn revault(&self, outpoint: &OutPoint) -> Result<(), RevaultDError> {
        self.handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .revault(*outpoint)
            .map_err(|e| e.into())
    }

    fn emergency(&self) -> Result<(), RevaultDError> {
        self.handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .emergency()
            .map_err(|e| e.into())
    }

    fn get_server_status(&self) -> Result<ServersStatuses, RevaultDError> {
        Ok(self
            .handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .get_servers_statuses())
    }

    fn get_history(
        &self,
        kind: &[HistoryEventKind],
        start: u32,
        end: u32,
        limit: u64,
    ) -> Result<Vec<HistoryEvent>, RevaultDError> {
        self.handle
            .as_ref()
            .ok_or(RevaultDError::NoAnswer)?
            .lock()
            .unwrap()
            .control
            .get_history(start, end, limit, kind)
            .map_err(|e| e.into())
    }
}
