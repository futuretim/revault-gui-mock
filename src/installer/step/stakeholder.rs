use std::cmp::Ordering;
use std::str::FromStr;
use bitcoin::Network::Bitcoin;
use bitcoin::secp256k1::Secp256k1;
use bitcoin::hashes::hex;

use bitcoin::util::bip32::{ExtendedPrivKey, ExtendedPubKey};
use iced::{Element, text_input};
use revaultd::revault_tx::{
    miniscript::DescriptorPublicKey,
    scripts::{DepositDescriptor, EmergencyAddress, UnvaultDescriptor},
};

use revault_ui::component::form;

use crate::installer::{
    config,
    message::{self, Message},
    step::{
        common::{CosignerKey, ParticipantXpub, RequiredXpub},
        Context, Step,
    },
    view,
};

pub struct DefineStakeholderXpubs {
    other_xpubs: Vec<ParticipantXpub>,
    our_xpub: RequiredXpub,
    warning: Option<String>,

    view: view::DefineStakeholderXpubsAsStakeholder,
}

impl DefineStakeholderXpubs {
    pub fn new() -> Self {
        Self {
            warning: None,
            our_xpub: RequiredXpub::new(),
            other_xpubs: Vec::new(),
            view: view::DefineStakeholderXpubsAsStakeholder::new(),
        }
    }
}

macro_rules! hex (($hex:expr) => (<Vec<u8> as bitcoin::hashes::hex::FromHex>::from_hex($hex).unwrap()));

impl Step for DefineStakeholderXpubs {
    fn update(&mut self, message: Message) {
        if let Message::DefineStakeholderXpubs(msg) = message {
            match msg {
                message::DefineStakeholderXpubs::OurXpubEdited(xpub) => {
                }
                message::DefineStakeholderXpubs::StakeholderXpub(
                    i,
                    message::ParticipantXpub::Delete,
                ) => {
                }
                message::DefineStakeholderXpubs::StakeholderXpub(i, msg) => {
                }
                message::DefineStakeholderXpubs::AddXpub => {
                }
            };
        };
    }

    fn apply(&mut self, ctx: &mut Context, config: &mut config::Config) -> bool {
        true
    }

    fn view(&mut self) -> Element<Message> {
        return self.view.render(
            &self.our_xpub.xpub,
            self.other_xpubs
                .iter_mut()
                .enumerate()
                .map(|(i, xpub)| {
                    xpub.view().map(move |msg| {
                        Message::DefineStakeholderXpubs(
                            message::DefineStakeholderXpubs::StakeholderXpub(i, msg),
                        )
                    })
                })
                .collect(),
            self.warning.as_ref(),
        );
    }
}

impl Default for DefineStakeholderXpubs {
    fn default() -> Self {
        Self::new()
    }
}

impl From<DefineStakeholderXpubs> for Box<dyn Step> {
    fn from(s: DefineStakeholderXpubs) -> Box<dyn Step> {
        Box::new(s)
    }
}

pub struct DefineManagerXpubs {
    managers_threshold: form::Value<usize>,
    spending_delay: form::Value<u32>,
    manager_xpubs: Vec<ParticipantXpub>,
    cosigners_enabled: bool,
    cosigners: Vec<CosignerKey>,
    warning: Option<String>,
    view: view::DefineManagerXpubsAsStakeholderOnly,

    /// from previous step
    stakeholder_xpubs: Vec<String>,
}

impl DefineManagerXpubs {
    pub fn new() -> Self {
        Self {
            managers_threshold: form::Value {
                value: 1,
                valid: true,
            },
            spending_delay: form::Value {
                value: 10,
                valid: true,
            },
            manager_xpubs: Vec::new(),
            cosigners_enabled: false,
            cosigners: Vec::new(),
            view: view::DefineManagerXpubsAsStakeholderOnly::new(),
            stakeholder_xpubs: Vec::new(),
            warning: None,
        }
    }
}
impl Step for DefineManagerXpubs {
    fn load_context(&mut self, ctx: &Context) {
        self.stakeholder_xpubs = ctx.stakeholders_xpubs.clone();
        self.cosigners_enabled = ctx.cosigners_enabled;
        while self.cosigners.len() != ctx.number_cosigners {
            match self.cosigners.len().cmp(&ctx.number_cosigners) {
                Ordering::Greater => {
                    self.cosigners.pop();
                }
                Ordering::Less => self.cosigners.push(CosignerKey::new()),
                Ordering::Equal => (),
            }
        }
    }

    fn update(&mut self, message: Message) {
        if let Message::DefineManagerXpubs(msg) = message {
            match msg {
                message::DefineManagerXpubs::ManagerXpub(i, message::ParticipantXpub::Delete) => {
                    self.manager_xpubs.remove(i);
                }
                message::DefineManagerXpubs::ManagerXpub(i, msg) => {
                    if let Some(xpub) = self.manager_xpubs.get_mut(i) {
                        xpub.update(msg)
                    };
                }
                message::DefineManagerXpubs::AddXpub => {
                    self.manager_xpubs.push(ParticipantXpub::new());
                }
                message::DefineManagerXpubs::CosignerKey(i, msg) => {
                    if let Some(key) = self.cosigners.get_mut(i) {
                        key.update(msg)
                    };
                }
                message::DefineManagerXpubs::CosignersEnabled(enable) => {
                    self.cosigners_enabled = enable
                }
                message::DefineManagerXpubs::ManagersThreshold(action) => match action {
                    message::Action::Increment => {
                        self.managers_threshold.valid = true;
                        self.managers_threshold.value += 1;
                    }
                    message::Action::Decrement => {
                        self.managers_threshold.valid = true;
                        if self.managers_threshold.value > 0 {
                            self.managers_threshold.value -= 1;
                        }
                    }
                },
                message::DefineManagerXpubs::SpendingDelay(action) => match action {
                    message::Action::Increment => {
                        self.spending_delay.value += 1;
                        self.spending_delay.valid = true;
                    }
                    message::Action::Decrement => {
                        self.spending_delay.valid = true;
                        if self.spending_delay.value > 0 {
                            self.spending_delay.value -= 1;
                        }
                    }
                },
                _ => {}
            };
        };
    }

    fn apply(&mut self, ctx: &mut Context, config: &mut config::Config) -> bool {
        true
    }

    fn view(&mut self) -> Element<Message> {
        let cosigners = if self.cosigners_enabled {
            self.cosigners
                .iter_mut()
                .enumerate()
                .map(|(i, key)| {
                    key.view().map(move |msg| {
                        Message::DefineManagerXpubs(message::DefineManagerXpubs::CosignerKey(
                            i, msg,
                        ))
                    })
                })
                .collect()
        } else {
            Vec::new()
        };
        self.view.render(
            &self.managers_threshold,
            &self.spending_delay,
            self.manager_xpubs
                .iter_mut()
                .enumerate()
                .map(|(i, xpub)| {
                    xpub.view().map(move |msg| {
                        Message::DefineManagerXpubs(message::DefineManagerXpubs::ManagerXpub(
                            i, msg,
                        ))
                    })
                })
                .collect(),
            cosigners,
            self.cosigners_enabled,
            self.warning.as_ref(),
        )
    }
}

impl Default for DefineManagerXpubs {
    fn default() -> Self {
        Self::new()
    }
}

impl From<DefineManagerXpubs> for Box<dyn Step> {
    fn from(s: DefineManagerXpubs) -> Box<dyn Step> {
        Box::new(s)
    }
}

pub struct DefineEmergencyAddress {
    address: form::Value<String>,
    warning: Option<String>,

    view: view::DefineEmergencyAddress,
}

impl DefineEmergencyAddress {
    pub fn new() -> Self {
        Self {
            address: form::Value::default(),
            view: view::DefineEmergencyAddress::new(),
            warning: None,
        }
    }
}

impl Step for DefineEmergencyAddress {
    fn update(&mut self, message: Message) {
    }

    fn apply(&mut self, _ctx: &mut Context, config: &mut config::Config) -> bool {
        true
    }

    fn view(&mut self) -> Element<Message> {
        self.view.render(&self.address, self.warning.as_ref())
    }
}

impl Default for DefineEmergencyAddress {
    fn default() -> Self {
        Self::new()
    }
}

impl From<DefineEmergencyAddress> for Box<dyn Step> {
    fn from(s: DefineEmergencyAddress) -> Box<dyn Step> {
        Box::new(s)
    }
}
